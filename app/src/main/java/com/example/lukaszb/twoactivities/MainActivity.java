package com.example.lukaszb.twoactivities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    private Button bNowa;
    private EditText etTekst;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bNowa=findViewById(R.id.bNowa);
        etTekst=findViewById(R.id.etTekst);

        bNowa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(etTekst.getText().length()>0) {
                    Intent i = new Intent(view.getContext(), SecondActivity.class);
                    String txt=etTekst.getText().toString();
                    i.putExtra("tekst",txt);
                    startActivity(i);
                }
            }
        });
    }
}
