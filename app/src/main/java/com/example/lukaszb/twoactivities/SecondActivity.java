package com.example.lukaszb.twoactivities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {
    private TextView tv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        tv=findViewById(R.id.tv);
        Intent i=getIntent();
        String txt=i.getStringExtra("tekst");
        if(txt.length()>0)
            tv.setText(txt);

    }
}
